const Modals = {
    NewEntry: "New Entry",
    ViewEntry: "View Entry"
};

var app = new Vue({
    el: "#basicjournal",
    data: {
        active: 0,
        tally: 0,
        entries: [],
        tempEntry: {
            id: -1,
            label: "",
            date: "",
            contents: ""
        },
        modal: {
            active: false,
            name: "",
            modified: false
        }
    },
    methods: {
        loadentries: function() {
            this.entries.length = 0;
            SamsApp.post("/", "get_entry_list", {}, function(response) {
                console.log(response);
                app.tally = response.data.tally;
                for(i = 0; i < response.data.entries.length; i++) {
                    app.entries.push({
                        id: i,
                        label: response.data.entries[i],
                        loaded: false,
                        date: "1970-1-1",
                        contents: "",
                    });
                }
            });
        },
        modal_modified: function(evt) {
            this.modal.modified = true;
        },
        modal_close: function() {
            if(this.modal.modified) {
                if(confirm("Your changes have not been saved.\n" + 
                "If you close this menu, they will be discarded.\n" + 
                "Close anyway?")) {
                    this.modal.active = false;
                } else {
                    return;
                }
            }
            this.modal.active = false;
        },
        newentry_open: function() {
            this.tempEntry.id = this.tally;
            this.tempEntry.label = "";
            this.tempEntry.date = SamsApp.asDateInput();
            this.tempEntry.contents = "";
            this.modal.name = Modals.NewEntry;
            this.modal.modified = false;
            this.modal.active = true;
        },
        newentry_submit: function() {
            SamsApp.post("/", "new_entry", {
                label: this.tempEntry.label,
                date: this.tempEntry.date,
                contents: this.tempEntry.contents
            }, function(response) {
                console.log(response);
                if(response.data.success == true) {
                    app.loadentries();
                    app.modal.modified = false;
                    app.modal_close();
                }
            });
        },
        loadentry: function(entryid, callback) {
            alert("loadentry called!");
            SamsApp.post("/", "get_entry", {
                id: entryid
            }, function(response) {
                console.log(response);
                for(entry of app.entries) {
                    if(entry.id == entryid) {
                        entry.date = response.data.date;
                        entry.contents = response.data.contents;
                        entry.loaded = true;
                        callback(entryid);
                    }
                }
            });
        },
        viewentry_open: function(entryid) {
            for(entry of this.entries) {
                if(entry.id == entryid) {
                    if(!entry.loaded) {
                        this.loadentry(entryid, this.viewentry_open);
                        return;
                    }
                    this.tempEntry.id = entryid;
                    this.tempEntry.label = entry.label;
                    this.tempEntry.date = entry.date;
                    this.tempEntry.contents = entry.contents;
                    this.modal.name = Modals.ViewEntry;
                    this.modal.modified = false;
                    this.modal.active = true;
                }
            }
        },
        viewentry_update: function() {
            SamsApp.post("/", "update_entry", this.tempEntry, function(response) {
                console.log(response);
                if(response.data.success) {
                    app.loadentries();
                    app.modal.modified = false;
                    app.modal_close();
                }
            });
        }
    }
});

app.loadentries();