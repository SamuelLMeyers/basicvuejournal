/**
 * samsapp.js
 * Common functions, classes, etc...
 */

const SamsApp = {
    /**
     * Sends a post request to the url.
     * @param {string} url The url we're "POST"ing.
     * @param {string} method The action we're asking the server to do.
     * @param {object} data The data we're sending.
     * @param {function(object)} onrecieve A callback function to be called when a response is received.
     */
    post: function(url, method, data, onrecieve) {
        var fd = new FormData();
        fd.set("method", method);
        for(field of Object.entries(data)) {
            fd.set(field[0], field[1]);
        }
        axios({
            method: "post",
            url: url,
            data: fd,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then((response) => {
            onrecieve(response);
        });
    },
    /**
     * Converts a Date object into a value accepted by <input type="date" />.
     * @param {Date} date The date object we're converting.  Defaults to the current date.
     */
    asDateInput: function(date=new Date()) {
        ds = date.getFullYear() + "-";
        if(date.getMonth() < 11) {
            ds += "0";
        }
        ds += (date.getMonth()+1) + "-";
        if(date.getDate() < 10) {
            ds += "0";
        }
        ds += date.getDate();
        return ds;
    }
};