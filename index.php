<?php

/*
    * NOTICE: NOT PART OF THE REAL APPLICATION.
    * TEMPORARY, POORLY WRITTEN PLACEHOLDER FOR
    * TESTING PURPOSES ONLY.
    * Seriously.  This damn thing not only writes
    * arbitrary data to the disk without any 
    * authentication or escaping WHATSOEVER, it 
    * doesn't even check to make sure the files
    * it needs exist.  I assume ABSOLUTELY NO
    * RESPONSIBILITY for anything that happens as
    * a result of this script being served on an
    * actual web server.
    */

if($_SERVER["REQUEST_METHOD"] == "GET") {
    echo(file_get_contents("html/home.html"));
    exit(0);
}

else{
    switch($_POST["method"]) {
        case "get_entry_list":
            echo(file_get_contents("data/entries.json"));
            break;
        case "get_entry":
            echo(file_get_contents("data/entry" . $_POST["id"] . ".json"));
            break;
        case "new_entry":
            $entryList = json_decode(file_get_contents("data/entries.json"), true);
            $entryId = count($entryList["entries"]);
            $entryList["entries"][$entryId] = $_POST["label"];
            $entry = [
                "date" => $_POST["date"],
                "contents" => $_POST["contents"]
            ];
            file_put_contents("data/entries.json", json_encode($entryList));
            file_put_contents("data/entry" . $entryId . ".json", json_encode($entry));
            $result = [
                "success" => true,
                "id" => $entryId
            ];
            echo(json_encode($result));
            break;
        case "update_entry":
            $entryList = json_decode(file_get_contents("data/entries.json"), true);
            $entry = json_decode(file_get_contents("data/entry" . $_POST["id"] . ".json"), true);
            $entryList["entries"][$_POST["id"]] = $_POST["label"];
            $entry["date"] = $_POST["date"];
            $entry["contents"] = $_POST["contents"];
            file_put_contents("data/entries.json", json_encode($entryList));
            file_put_contents("data/entry" . $_POST["id"] . ".json", json_encode($entry));
            $result = [
                "success" => true
            ];
            echo(json_encode($result));
            break;
        default:
            echo(json_encode([
                "success" => false
            ]));
    }
}
?>
